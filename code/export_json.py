import pandas as pd
import numpy as np
import snownlp as nlp
import os
import json
import collections
from tqdm import tqdm
from snownlp import SnowNLP
import ast
#from collections import Counter

#class ExportJson:


# class CoOccurrence(object):
#     def Analysis(self, excerpt):
#         s = SnowNLP(excerpt)
#         x = s.sentences
#         print(excerpt)
#         t = CoOccurrence.Test("test")
#         return "test"

#     def Test(test):
#         sentence='吃葡萄不吐葡萄皮，不吃葡萄倒吐葡萄皮。'
#         chlist=[ch for ch in sentence]

#         print(chlist)
#         chfreqdict = CoOccurrence.list2freqdict(chlist)
#         print(chfreqdict)
#         return test

#     def list2freqdict(mylist):
#         mydict=dict()
#         for ch in mylist:
#             mydict[ch] = mydict.get(ch,0) + 1
#         return mydict





def main():
    print('main()...')
    doExport('week','week_detail','week','Week')
    #doExport('month','month_detail','month','Month')
    # try:

    # except:
    #     print("Execution Error....")
    # finally:
    #     print("Completed....")


def doExport(fromfolder,tofolder,path_type,path_cs_type):
    exceptionMSG_str = ""
    path_str = os.path.join('..','output',fromfolder)
    checkPath_str = os.path.join('..','output',tofolder)
    #path_str = "C:\\Users\\g-remtsai\\Documents\\dcard_analysis\\output\\week\\"
    #checkPath_str = "C:\\Users\\g-remtsai\\Documents\\dcard_analysis\\output\\week_detail\\"


    print(len(os.listdir(path_str)))

    #try:
    for ff in tqdm(os.listdir(path_str)):

            print(ff)
            if os.path.isfile(os.path.join(path_str, ff)):

                #if ff != 'All_Week.json':
                    df = pd.read_json(os.path.join(path_str, ff))
                    ds = pd.DataFrame(df, columns = ['kWordID', 'kWord', 'kWordCount', 'fType', 'aFlag'])
                    #ds.sort_values(by = ['kWordID'], inplace = True, ascending = False)
                    print(df.shape[0])

                    for index, row in ds.iterrows():
                        #if(index>5):
                        #    break

                        #d_type_month="Month"
                        #d_type_week="Week"

                        #s_type_month='month'
                        #s_type_week='week'

                        if(index>50 and ff != '遊戲_'+path_cs_type+'.json'):
                            break

                        checkfile_str = str(row['kWordID']) + '_'+path_type+'.json'

                        if os.path.isfile(os.path.join(checkPath_str, checkfile_str)):
                            print(checkfile_str,' exist , skip')
                            continue
                        else:
                            #暫時測試用
                                #continue
                                #exceptionMSG_str = path_str + ff + " : " + str(row['kWordID']) + " Error"
                                #print(index)
                                OpenOut(row['kWordID'], row['kWord'], row['aFlag'],checkPath_str,path_type)




def OpenOut(kWordID, kWord, aFlag,path_out,path_type):
    print('## OpenOut ',kWordID,'  ',kWord,'  ',aFlag)
    exceptionMSG_str = ""
    gender_str = []
    school_str = []
    forumAlias_str = []
    forumName_str = []
    forumId_str = []
    nlp_str = []
    analysis_str = []
    #x = CoOccurrence()


    path_str = os.path.join('..','output')

    try:
        for file in os.listdir(path_str):
            if os.path.isfile(os.path.join(path_str, file)):

                df = pd.read_csv(os.path.join(path_str, file))
                ds = pd.DataFrame(df, columns = ['id', 'forumAlias', 'forumName', 'title', 'excerpt', 'topics', 'gender', 'school', 'likeCount', 'createdAt', 'updatedAt', 'extttt'])

                for index, row in ds.iterrows():
                    if IsNaN(row['extttt']) == False:
                        keywordStr = row['extttt'].replace('[', '').replace(']', '').replace('\'', '')
                        keyword_str = keywordStr.split(',')
                        exceptionMSG_str = path_str + file + " : " + str(row['id']) + " Error"

                        for keywordCheck in keyword_str:
                            kStr = keywordCheck.replace(' ', '')

                            if kStr == kWord:
                                gender_str += SetGender(row['gender'])
                                school_str += SetSchool(row['school'])
                                forumAlias_str += SetForumAlias(row['forumAlias'])
                                forumName_str += SetForumName(row['forumName'])
                                forumId_str += SetForumId(row['forumName'],row['forumAlias'], row['title'], row['id'])
                                nlp_str += SetNLP(row["excerpt"])
                                #analysis_str += x.Analysis(row["excerpt"])
                                #print(kStr)
    except:
        print(exceptionMSG_str)


    genderCountList = collections.Counter(gender_str)
    genderJson = ReSetJsonFormat(genderCountList, '')

    schoolCountList = collections.Counter(school_str)
    schoolJson = ReSetJsonFormat(schoolCountList, '')
    schoolJson = sorted(schoolJson, key = lambda k: k.get('value', 0), reverse = True)

    forumAliasCountList = collections.Counter(forumAlias_str)
    forumNameCountList = collections.Counter(forumName_str)
    forumAliasJson = ReSetJsonFormat(forumAliasCountList, forumId_str)

    nlpCountList = collections.Counter(nlp_str)
    nlpJson = ReSetJsonFormat(nlpCountList, '')

    #未完成
    analysisList = SetRelatedWord("")
    analysisJson = analysisList

    getJson = SetJson(path_type,kWordID, kWord, aFlag, forumAliasJson, genderJson, schoolJson, nlpJson, analysisJson)

    WriteJson(kWordID, path_out, getJson,path_type)
    #print(getJson)


def SetRelatedWord(relatedWord):
    aList = []
    #aList.append(relatedWord)
    return aList


def SetNLP(excerpt):
    moodStr = ""

    s = SnowNLP(excerpt)
    if s.sentiments < 0.05:
        moodStr = "極負面"
    elif s.sentiments > 0.95:
        moodStr = "極正面"
    elif s.sentiments <= 0.4:
        moodStr = "負面"
    elif s.sentiments <= 1 and s.sentiments >= 0.7:
        moodStr = "正面"
    elif s.sentiments < 0.7 and s.sentiments > 0.4:
        moodStr = "中立"

    aList = []
    aList.append(moodStr)
    return aList


def SetGender(gender):
    aList = []
    aList.append(gender)
    return aList


def SetSchool(school):
    lastChars = school[-2:]

    if lastChars != "大學" and lastChars != "學院":
        school = "其他"

    aList = []
    aList.append(school)
    return aList


def SetForumAlias(forumAlias):
    aList = []
    aList.append(forumAlias)
    return aList

def SetForumName(forumName):
    aList = []
    aList.append(forumName)
    return aList

def SetForumId(forumName,forumAlias, forumTitle, forumId):
    aList = []
    aList.append({
        'name':forumName,
        'key': forumAlias,
        'title': forumTitle,
        'value': int(forumId)
        })
    return aList


def ReSetJsonFormat(val, key):
    vl = list(val)
    kl = list(key)
    print(vl)
    print(kl)
    json_str = []

    if key == '':
        for i in vl:
            json_str.append({
                'key': i,
                'value': int(val[i])
            })
    else:
        for i in range(len(vl)):
            subkey = []
            fname = ''

            for j in kl:
                if vl[i] == str(j['key']):
                    fname=str(j['name'])
                    subkey.append({
                        'title': str(j['title']),
                        'value': int(j['value'])
                    })

            #print(kl[i])
            #print(type(kl[i]))
            json_str.append({
                'name':fname,
                'key': vl[i],
                'value': len(subkey),
                'title': subkey
            })
    #print('ReSetJsonFormat:',json_str)
    return json_str

def SetJson(type,kWordID, kWord, aFlag, forumAlias, gender, school, nlp, relatedWord):
    data = {}
    data[type] = []
    data[type].append({
        'kwID': kWordID,
        'kWord': kWord,
        'aFlag': aFlag,
        'mood': nlp ,
        'forumType': forumAlias,
        'sex': gender,
        'school': school,
        'relevance': relatedWord
    })

    return data


def WriteJson(kWordID, path, data,path_type):
    write_path = path
    if os.path.isdir(write_path) == False:
        os.makedirs(path)

    filePathNameWExt =  os.path.join(write_path, str(kWordID)+'_'+path_type+'.json')
    with open(filePathNameWExt, 'w') as outfile:
        json.dump(data, outfile)

    os.close


def IsNaN(string):
    return string != string

if __name__ == "__main__":
    main()





