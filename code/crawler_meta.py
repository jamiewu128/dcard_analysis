from dcard import Dcard
import pandas as pd
import datetime as dt
import time
import os

def get_delta_datetime(delta_day=1):
    datetime_format = (dt.datetime.today()-dt.timedelta(days=delta_day)).strftime("%Y-%m-%dT%H:%M:%S")
    print('##Timebound: ',datetime_format)
    return datetime_format

path_output=os.path.join('..','metas',dt.datetime.today().strftime("%Y-%m-%d-%H-%M-%S"))
path_before_ids='before_ids.csv'

df=pd.read_csv(os.path.join('',path_before_ids))
fnames=list(df['forums'])
alias=list(df['alias'])
last_ids=list(df['beforeId'])
#print(fnames)
print(len(alias))
print(type(alias))


# Timebound Date
delta_day=1

# Find newest post id
beforeId=0
originBeforeId=0
dcard = Dcard()

#newest_ids=[]
#for a in alias:
for i in range(len(alias)):

    print(fnames[i])

    while True:
        ariticle_metas = dcard.forums(alias[i]).get_metas(num=100,sort='new',timebound=get_delta_datetime(delta_day))
        total_get=len(ariticle_metas)
        print('get count: ',total_get)
        if total_get>0:
            beforeId=ariticle_metas[0].get('id')
            beforeId=beforeId+1
            print('newest post id: ',beforeId)
            break
        else:
            delta_day=delta_day+1
            print('delta days=',delta_day)
            time.sleep(1)

    amlist=[]

    csv_name=alias[i]+'_metas.csv'

    originBeforeId=last_ids[i]
    print('lastest keep id: ',originBeforeId)

    while True:

        ariticle_metas = dcard.forums(alias[i]).get_metas(num=1,sort='new',before=beforeId)
        total_get=len(ariticle_metas)
        print('metas count: ',total_get)

        if total_get<=0:
            break
        else:

            title=ariticle_metas[len(ariticle_metas)-1].get('title')
            datetime_format=ariticle_metas[len(ariticle_metas)-1].get('updatedAt')
            beforeId=ariticle_metas[len(ariticle_metas)-1].get('id')
            print('id: ',beforeId,'  date: ',datetime_format) #,'  title: ',title)

            if beforeId<=originBeforeId:
                break
            else:

                amlist.append(pd.DataFrame(data=ariticle_metas))
                print('Total now: ',len(amlist))

        totlelen=len(amlist)
        print('## ',alias[i],' ##  Total: ',totlelen)
        #if originBeforeId==0 & totlelen>1001:
        #    break
        time.sleep(1)

    ardf = pd.concat(amlist,axis=0)
    if os.path.exists(path_output)==False:
        os.makedirs(path_output)

    ardf.to_csv(os.path.join(path_output,csv_name),encoding='utf_8_sig')
    #newest_ids.append(ardf['id'].iloc[0])

    last_ids[i]=ardf['id'].iloc[0]
    last_id_df=pd.DataFrame({'forums':fnames,'alias':alias,'beforeId':last_ids})
    last_id_df.to_csv('before_ids.csv',encoding='utf_8_sig')




